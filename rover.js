//private variables
var X;
var Y;
var Heading;

//constructors
function Rover(x, y, heading){
	X = x;
	Y = y;
	Heading = heading;
}

//methods
Rover.prototype.move = function() {
	if(Heading == "N")
	{
		Y++;
	}
	else if(Heading == "S")
	{
		Y--;
	}
	else if(Heading == "E")
	{
		X++;
	}
	else if(Heading == "W")
	{
		X--;
	}
}

Rover.prototype.isOutOfBounds = function(gridSize) {

	return X < 0 || X > gridSize[0] || Y < 0 || Y > gridSize[1];
}

Rover.prototype.rotate = function(turnDirection) {
	var directions = ["N", "E", "S", "W"];

	var directionValue = directions.indexOf(Heading);

	if(turnDirection == "R")
	{
		if(directionValue + 1 > 3)
			Heading = directions[0];
		else	
			Heading = directions[directionValue + 1];
	}
	else if(turnDirection == "L")
	{
		if(directionValue - 1 < 0)
			Heading = directions[3];
		else	
			Heading = directions[directionValue - 1];
	}
}


//properties
Rover.prototype.getX = function() {
	return X;
}

Rover.prototype.getY = function() {
	return Y;
}

Rover.prototype.getHeading = function() {
	return Heading;
}

module.exports = Rover; 