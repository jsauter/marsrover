var Rover = require("./rover")
var inputParser = require("./inputParser.js")


var execute = function(input) {

	var parsedInitialInput = inputParser.ParseInitialInput(input);

	var output = "";
	var gridSize = parsedInitialInput[0]

	for (var i = 1; i < parsedInitialInput.length; i++) { 

    	output = output + processRover(gridSize, parsedInitialInput[i], parsedInitialInput[i + 1]) + "\n";

    	i++;
	}

	return output;
}

var processRover = function(gridSize, startingCoordinates, commands) {
	var parsedSize = inputParser.ParseInitialGridSize(gridSize);	
	var parsedStartingCoordinates = inputParser.ParseInitialCoordinates(startingCoordinates);
	var parsedCommands = inputParser.ParseCommandInput(commands);

	var rover = new Rover(parsedStartingCoordinates[0], parsedStartingCoordinates[1], parsedStartingCoordinates[2])

	parsedCommands.forEach(function(command) {
		if(command == "M")
		{
			var newPos = rover.move();
		}
		else if(command == "L" || command == "R")
		{			
			rover.rotate(command);			
		}
		else
		{
			throw new Error("Unsupported Command")
		}

		if(rover.isOutOfBounds(parsedSize)) {
			throw new Error("Rover left exploration area and is lost");
		}
	})

	return rover.getX() + " " + rover.getY() + " " + rover.getHeading();
}

module.exports.Execute = execute;
module.exports.ProcessRover = processRover;