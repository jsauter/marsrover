var assert = require("assert")
var parser = require("../inputParser.js")

describe('input parser tests', function(){
	describe('test command parse', function(){
		it('LML returns ["L", "M", "L"]', function(){

			var parsedString = parser.ParseCommandInput("LML");

			assert.equal(parsedString[0], "L");
			assert.equal(parsedString[1], "M");
			assert.equal(parsedString[2], "L");

		})
	}),
	describe('test initial coordinates parse', function(){
		it('1 2 N returns ["1", "2", "N"]', function(){

			var parsedString = parser.ParseInitialCoordinates("1 2 N");
			
			assert.equal(parsedString[0], "1");
			assert.equal(parsedString[1], "2");
			assert.equal(parsedString[2], "N");

		})
	}),
	describe('test initial grid size parse', function(){
		it('5 5 returns ["5","5"]', function(){

			var parsedString = parser.ParseInitialCoordinates("5 5");
			
			assert.equal(parsedString[0], "5");
			assert.equal(parsedString[1], "5");

		})
	}), 
	describe('test parse of inital input', function() {
		it('5 5\n 3 3 N\n MLMLM returns three string array', function() {
			var parsedString = parser.ParseInitialInput("5 5\n 3 3 N\n MLMLM");

			assert.equal(parsedString.length, 3);
		}),
		it('5 5\n 3 3 N\n MLMLM\n 2 2 E\n MLMLM returns five string array', function() {
			var parsedString = parser.ParseInitialInput("5 5\n 3 3 N\n MLMLM\n 2 2 E\n MLMLM");

			assert.equal(parsedString.length, 5);
		})
	})
})
