var assert = require("assert")
var orchestrator = require("../roverOrchestrator.js")

describe('functional tests', function(){
	describe('test end to end', function(){
		it('input of /5 5 /1 2 N /LMLMLMLMM returns 1 3 N', function(){
			var result = orchestrator.ProcessRover("5 5", "1 2 N", "LMLMLMLMM");
			assert.equal(result, "1 3 N");
		}),
		it('input of /5 5 /3 3 E /MMRMMRMRRM returns 5 1 E', function(){
			var result = orchestrator.ProcessRover("5 5", "3 3 E", "MMRMMRMRRM");
			assert.equal(result, "5 1 E");
		})
	}),
	describe('test full input use case', function() {
		it('if full normal input should output expected', function() {
			var result = orchestrator.Execute("5 5\n1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM");
			assert.equal(result, "1 3 N\n5 1 E\n")
		})
	})
	describe('test error in input', function() {

		it('invalid input throws Unsupported Command error', function() {
			assert.throws(orchestrator.ProcessRover, Error, "Unsupported Command");	
		}),
		it('rover leaves exloration area throws exception', function() {
			assert.throws(orchestrator.ProcessRover, Error, "Rover left exploration area and is lost");	
		})				
	})
})
