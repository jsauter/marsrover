var assert = require("assert");
var Rover = require("../rover");

describe('rover tests', function(){

	//constructor test
	describe('test create rover', function(){
		it('rover should be created with default vaules', function(){
			var rover = new Rover(1, 2, "N");

			assert.equal(rover.getX(), 1);
			assert.equal(rover.getY(), 2);
			assert.equal(rover.getHeading(), "N");
		})
	}),

	// rover moving tests
	describe('test move rover various places', function(){
		it('when [1,1] rover should move north when commanded to [1,2]', function() {
			
			var rover = new Rover(1, 1, "N"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 1);
			assert.equal(rover.getY(), 2); 
		}),
		it('when [1,1] rover should move south when commanded to [1,0]', function() {
		
			var rover = new Rover(1, 1, "S"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 1);
			assert.equal(rover.getY(), 0); 
		}),
		it('when [1,1] rover should move east when commanded to [2,1]', function() {
		
			var rover = new Rover(1, 1, "E"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 2);
			assert.equal(rover.getY(), 1); 
		}),
		it('when [1,1] rover should move west when commanded to [0,1]', function() {
		
			var rover = new Rover(1, 1, "W"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 0);
			assert.equal(rover.getY(), 1); 
		}),
		it('when [0,1] rover should move north when commanded to [0,2]', function() {
			
			var rover = new Rover(0, 1, "N"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 0);
			assert.equal(rover.getY(), 2); 
		}),
		it('when [0,1] rover should move south when commanded to [0,0]', function() {
		
			var rover = new Rover(0, 1, "S"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 0);
			assert.equal(rover.getY(), 0); 
		}),
		it('when [0,1] rover should move east when commanded to [1,1]', function() {
		
			var rover = new Rover(0, 1, "E"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 1);
			assert.equal(rover.getY(), 1); 
		}),
		it('when [0,1] rover should move west when commanded to [2,1]', function() {
		
			var rover = new Rover(0, 1, "W"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), -1);
			assert.equal(rover.getY(), 1); 
		}),	
		it('when [1,0] rover should move north when commanded to [1,1]', function() {
			
			var rover = new Rover(1, 0, "N"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 1);
			assert.equal(rover.getY(), 1); 
		}),
		it('when [1,0] rover should move south when commanded to [1,-1]', function() {
		
			var rover = new Rover(1, 0, "S"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 1);
			assert.equal(rover.getY(), -1); 
		}),
		it('when [1,0] rover should move east when commanded to [2,0]', function() {
		
			var rover = new Rover(1, 0, "E"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 2);
			assert.equal(rover.getY(), 0); 
		}),
		it('when [1,0] rover should move west when commanded to [0,0]', function() {
		
			var rover = new Rover(1, 0, "W"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 0);
			assert.equal(rover.getY(), 0); 
		}),

		//////////////////////////////////////
	
		it('when [0,0] rover should move north when commanded to [0,1]', function() {
			
			var rover = new Rover(0, 0, "N"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 0);
			assert.equal(rover.getY(), 1); 
		}),
		it('when [0,0] rover should move south when commanded to [0,-1]', function() {
		
			var rover = new Rover(0, 0, "S"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 0);
			assert.equal(rover.getY(), -1); 
		}),
		it('when [0,0] rover should move east when commanded to [1,0]', function() {
		
			var rover = new Rover(0, 0, "E"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), 1);
			assert.equal(rover.getY(), 0); 
		}),
		it('when [0,0] rover should move west when commanded to [-1,0]', function() {
		
			var rover = new Rover(0, 0, "W"); 

			var newPosition = rover.move();

			assert.equal(rover.getX(), -1);
			assert.equal(rover.getY(), 0); 
		})

	}),
	
	//rover rotation tests	
	describe('test rotate north', function(){
	it('should turn east when rotate right from north', function(){
		var rover = new Rover(0,0,"N");

		rover.rotate("R");

		assert.equal(rover.getHeading(), "E");

	}),
	it('should turn west when rotate left from north', function(){
		var rover = new Rover(0,0,"N");

		rover.rotate("L");

		assert.equal(rover.getHeading(), "W");

	})
	}),
	describe('test rotate south', function(){
		it('should turn west when rotate left from south', function(){
			
			var rover = new Rover(0,0,"S");

			rover.rotate("L");

			assert.equal(rover.getHeading(), "E");

		}),
		it('should turn east when rotate right from south', function(){
			
			var rover = new Rover(0,0,"S");

			rover.rotate("R");

			assert.equal(rover.getHeading(), "W");

		})	
	}),
	describe('test rotate east', function(){
		it('should turn north when rotate left from east', function(){

			var rover = new Rover(0,0,"E");

			rover.rotate("L");

			assert.equal(rover.getHeading(), "N");

		}),
		it('should turn south when rotate right from east', function(){

			var rover = new Rover(0,0,"E");

			rover.rotate("R");

			assert.equal(rover.getHeading(), "S");		

		})
	}),
	describe('test rotate west', function(){
		it('should turn south when rotate left from west', function(){
			
			var rover = new Rover(0,0,"W");

			rover.rotate("L");

			assert.equal(rover.getHeading(), "S");

		}),
		it('should turn north when rotate right from west', function(){
			
			var rover = new Rover(0,0,"W");

			rover.rotate("R");

			assert.equal(rover.getHeading(), "N");
		})
	}),
	describe('rotate full circle tests', function() {
		it('rover should turn in a circle when pointing north and turning right', function() {
			var rover = new Rover(0,0,"N");

			rover.rotate("R");
			assert.equal(rover.getHeading(), "E");

			rover.rotate("R");
			assert.equal(rover.getHeading(), "S");

			rover.rotate("R");
			assert.equal(rover.getHeading(), "W");

			rover.rotate("R");
			assert.equal(rover.getHeading(), "N");
		}),
		it('rover should turn in a circle when pointing north and turning left', function() {
			var rover = new Rover(0,0,"N");

			rover.rotate("L");
			assert.equal(rover.getHeading(), "W");

			rover.rotate("L");
			assert.equal(rover.getHeading(), "S");

			rover.rotate("L");
			assert.equal(rover.getHeading(), "E");

			rover.rotate("L");
			assert.equal(rover.getHeading(), "N");
		})
	}),

	//Out of bounds tests
	describe('test out of bounds', function() {
		it('should return false if in bounds', function() {
			var rover = new Rover(0,0,"W");

			assert.equal(rover.isOutOfBounds([5,5]),false)
		})
	}),
	describe('test out of bounds', function() {
		it('should return false if in bounds', function() {
			var rover = new Rover(-1,-1,"W");

			assert.equal(rover.isOutOfBounds([5,5]),true)
		})
	}),
	describe('test out of bounds', function() {
		it('should return false if in bounds', function() {
			var rover = new Rover(6,6,"W");

			assert.equal(rover.isOutOfBounds([5,5]),true)
		})
	})
})