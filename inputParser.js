var parseCommandInput = function(commandString){

	return commandString.split("");
}

var parseInitialCoordinates = function(initialCoordinates) {
	return initialCoordinates.split(" ");
}

var parseInitialGridSize = function(initialSize) {
	return initialSize.split(" ");
}

var parseInitialInput = function(initialInput) {
	return initialInput.split("\n");
}

module.exports.ParseCommandInput = parseCommandInput;
module.exports.ParseInitialCoordinates = parseInitialCoordinates
module.exports.ParseInitialGridSize = parseInitialGridSize
module.exports.ParseInitialInput = parseInitialInput