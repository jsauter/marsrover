# Mars Rover Coding Challenge!

Please see the code in this repository for the Mars Rover coding challenge.

I have used node.js as the framework for the application to run under and it should be pretty simple to get going.  There are no external dependencies other than Mocha if you want to run the unit tests.

### How to run:

1. Install Node.js on your computer
2. Pull down repo from bitbucket
3. Go to the root folder of the source and run "node app.js" (you can also run just ./app.js if you are in a linux environment if you make it executable with 'chmod +x')

### Unit Tests:

You can run the unit tests by installing Mocha globally and running: "node node_modules/mocha/bin/mocha" or adding to path and running just 'mocha'

How to install Mocha:

npm install -g mocha

Make sure that the npm folder is in your path.  For example... /usr/local/share/npm/bin in my case on OS X.

![beep boop beep](http://mars.nasa.gov/images/msl20110602_PIA14175-fi.jpg)
